# jvn

Check vulnerability from jvndb database

[JVN iPedia - 脆弱性対策情報データベース](https://jvndb.jvn.jp/)

## class-daiagram

Red boxes are not implemented yet.

```mermaid
flowchart TB
subgraph jvn
    subgraph jvn_api
        VulnOverview
        VulnOverviewListApi
        VulnOverviewListRequest
        VulnOverviewListXml

        VulnOverviewListApi --> VulnOverview
        VulnOverviewListApi --> VulnOverviewListRequest
        VulnOverviewListApi --> VulnOverviewListXml
        VulnOverviewListXml --> VulnOverview

        VulnDetailInfo
        VulnDetailInfoApi
        VulnDetailInfoRequest
        VulnDetailInfoXml

        VulnDetailInfoApi --> VulnDetailInfo
        VulnDetailInfoApi --> VulnDetailInfoRequest
        VulnDetailInfoApi --> VulnDetailInfoXml
        VulnDetailInfoXml --> VulnDetailInfo
    end
    subgraph domain
        Vuln["
            Vuln
            ---
            vuln_id
            description
            ...
        "]
        Target["
            Target
            ---
            product
            keyword
        "]
        Safelist["
            Safelist
            ---
            product
            vuln_id
        "]
        CheckVulnInfoResult["
            CheckVulnInfoResult
            ---
            product
            vuln_id
            result
        "]
    end
    subgraph repository
        TargetCsv
        SafelistCsv
        VulnInfoJvnApi
    end
    subgraph presentation
        CheckVulnInfoResultCsv
    end
    subgraph usecase
        CheckVulnInfo
    end
    CheckVulnInfo --> TargetCsv
    CheckVulnInfo --> SafelistCsv
    CheckVulnInfo --> VulnInfoJvnApi
    CheckVulnInfo --> CheckVulnInfoResultCsv
    VulnInfoJvnApi --> VulnOverviewListApi
    VulnInfoJvnApi --> VulnDetailInfoApi
    VulnInfoJvnApi --> Vuln
    TargetCsv --> Target
    SafelistCsv --> Safelist
    CheckVulnInfoResultCsv --> CheckVulnInfoResult

    style CheckVulnInfo fill:red,color:white
    style TargetCsv fill:red,color:white
    style Target fill:red,color:white
    style SafelistCsv fill:red,color:white
    style Safelist fill:red,color:white
    style CheckVulnInfoResultCsv fill:red,color:white
    style CheckVulnInfoResult fill:red,color:white
end
```

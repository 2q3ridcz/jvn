# APIを試してみる

`ubuntu`の脆弱性をAPIで検索する。

[JVN iPedia](https://jvndb.jvn.jp/search/index.php?mode=_vulnerability_search_IA_VulnSearch&lang=ja)で`ubuntu`を検索すると、`ubuntu`以外の脆弱性がたくさんHitしてしまうため、[MyJVN API](https://jvndb.jvn.jp/apis/index.html)を試してみる。

### getProductList

getProductList「製品一覧の取得」を要求する。  

`ubuntu`の場合は1回の要求で十分だが、1回で足りない場合はコードの`#request`以降を複数回実施することですべての応答を受け取ることができる。

```python
import requests
from bs4 import BeautifulSoup

keyword = "ubuntu"
product_list = []
start_item = 1

# request
url = f"https://jvndb.jvn.jp/myjvn?method=getProductList&feed=hnd&startItem={start_item}&keyword={keyword}"
res = requests.get(url=url, timeout=1)
text = res.text
soup = BeautifulSoup(text, features='xml')

# check status for error
status_list = soup.find_all(name='status:Status')
if len(status_list) != 1:
    raise RuntimeError(f"Unexpected status count. Count: {len(status_list)}, Status: {status_list}")

status = status_list[0]
return_code = int(status.get(key="retCd"))
if return_code != 0:
    raise RuntimeError(f'Unexpected return code. Code: {return_code}, Error code: {status.get(key="errCd")}, Error message: {status.get(key="errMsg")}')

# collect product
product_list += list(soup.find_all("mjres:Vendor"))

# check status to handle continue or break
total_response = int(status.get(key="totalRes"))
first_response = int(status.get(key="firstRes"))
total_response_returned = int(status.get(key="totalResRet"))
last_response = first_response + total_response_returned - 1
start_item = last_response + 1
if total_response <= last_response:
    print(f"Received all results. ({last_response} / {total_response})")
else:
    print(f"Received {last_response} results. Continue to receive more. (All: {total_response})")

```

すべての応答を受け取った後で`print(product_list)`を実行すると、製品の候補を表示できる。VenderとProductの組み合わせを確認し、自分の目的の製品の製品ID（pid）をメモする。

今回は目的に近い（と思った）"5112", "16904", "26077"を対象に選んだ。

```
>>> print(product_list)
[<mjres:Vendor cpe="cpe:/:canonical" vid="281" vname="Canonical">
（略）
<mjres:Product cpe="cpe:/o:canonical:ubuntu" pid="5112" pname="Ubuntu"/>
<mjres:Product cpe="cpe:/o:canonical:ubuntu_linux" pid="16904" pname="Ubuntu"/>
（略）
<mjres:Product cpe="cpe:/o:canonical:ubuntu_core" pid="26077" pname="Ubuntu Core"/>
（略）
</mjres:Vendor>]
```

### getVulnOverviewList

`getVulnOverviewList`「脆弱性対策概要情報一覧の取得」を要求する。  

`getProductList`で取得した製品IDを検索条件にする。  
脆弱性の詳細は`getVulnDetailInfo`で取得するため、ここでは脆弱性IDのみ取得する。

1回の応答上限が50なのに対して、`ubuntu`の全量は1396あった。コードの`#request`以降を28回実施するとすべての応答を受け取ることができる。今回はお試しなので、5回（250）にとどめたが、必要に応じて反復処理を組むとよい。

```python
import requests
from bs4 import BeautifulSoup

product_id = "+".join(["5112", "16904", "26077",])
vuln_overview_list = []
start_item = 1

# request
url = f"https://jvndb.jvn.jp/myjvn?method=getVulnOverviewList&feed=hnd&startItem={start_item}&productId={product_id}&rangeDatePublic=n&rangeDatePublished=n&rangeDateFirstPublished=n"
res = requests.get(url=url, timeout=1)
text = res.text
soup = BeautifulSoup(text, features='xml')

# check status for error
status_list = soup.find_all(name='status:Status')
if len(status_list) != 1:
    raise RuntimeError(f"Unexpected status count. Count: {len(status_list)}, Status: {status_list}")

status = status_list[0]
return_code = int(status.get(key="retCd"))
if return_code != 0:
    raise RuntimeError(f'Unexpected return code. Code: {return_code}, Error code: {status.get(key="errCd")}, Error message: {status.get(key="errMsg")}')

# collect vuln
vuln_overview_list += [x.text for x in soup.find_all("sec:identifier")]

# check status to handle continue or break
total_response = int(status.get(key="totalRes"))
first_response = int(status.get(key="firstRes"))
total_response_returned = int(status.get(key="totalResRet"))
last_response = first_response + total_response_returned - 1
start_item = last_response + 1
if total_response <= last_response:
    print(f"Received all results. ({last_response} / {total_response})")
else:
    print(f"Received {last_response} results. Continue to receive more. (All: {total_response})")

```

`print(vuln_overview_list)`で脆弱性IDを表示できる。

### getVulnDetailInfo

`getVulnDetailInfo`「脆弱性対策詳細情報の取得」を要求する。  

`getVulnOverviewList`で取得した脆弱性IDを検索条件にする。

今回のコードも、1回の要求で足りない場合は`#request`以降を複数回実施することですべての応答を受け取ることができる。

```python
import requests
from bs4 import BeautifulSoup

vuln_id = "+".join(vuln_overview_list)
vuln_detail_list = []
start_item = 1

# request
url = f"https://jvndb.jvn.jp/myjvn?method=getVulnDetailInfo&feed=hnd&startItem={start_item}&vulnId={vuln_id}"
res = requests.get(url=url, timeout=1)
text = res.text
soup = BeautifulSoup(text, features='xml')

# check status for error
status_list = soup.find_all(name='status:Status')
if len(status_list) != 1:
    raise RuntimeError(f"Unexpected status count. Count: {len(status_list)}, Status: {status_list}")

status = status_list[0]
return_code = int(status.get(key="retCd"))
if return_code != 0:
    raise RuntimeError(f'Unexpected return code. Code: {return_code}, Error code: {status.get(key="errCd")}, Error message: {status.get(key="errMsg")}')

# collect vuln_detail
vuln_detail_list += list(soup.find_all("vuldef:Vulinfo"))

# check status to handle continue or break
total_response = int(status.get(key="totalRes"))
first_response = int(status.get(key="firstRes"))
total_response_returned = int(status.get(key="totalResRet"))
last_response = first_response + total_response_returned - 1
start_item = last_response + 1
if total_response <= last_response:
    print(f"Received all results. ({last_response} / {total_response})")
else:
    print(f"Received {last_response} results. Continue to receive more. (All: {total_response})")

```

`print(vuln_detail_list)`で脆弱性詳細を表示できる。

`Ubuntu`の場合はVersionNumberに何も設定されない？自分のバージョンが該当するかを判断できない…残念。

```
（略）
<vuldef:Affected>
<vuldef:AffectedItem>
<vuldef:Name>Canonical</vuldef:Name>
<vuldef:ProductName>Ubuntu</vuldef:ProductName>
<vuldef:Cpe version="2.2">cpe:/o:canonical:ubuntu_linux</vuldef:Cpe>
<vuldef:VersionNumber/>
</vuldef:AffectedItem>
（略）
</vuldef:Affected>
（略）
```


## 蛇足：getVendorList

結果として今回は必要なかったが、備忘として記載。

```python
import requests
from bs4 import BeautifulSoup

vender_list = []
start_item = 1

# request
url = f"https://jvndb.jvn.jp/myjvn?method=getVendorList&feed=hnd&startItem={start_item}"
res = requests.get(url=url, timeout=1)
text = res.text
soup = BeautifulSoup(text, features='xml')

# check status for error
status_list = soup.find_all(name='status:Status')
if len(status_list) != 1:
    raise RuntimeError(f"Unexpected status count. Count: {len(status_list)}, Status: {status_list}")

status = status_list[0]
return_code = int(status.get(key="retCd"))
if return_code != 0:
    raise RuntimeError(f'Unexpected return code. Code: {return_code}, Error code: {status.get(key="errCd")}, Error message: {status.get(key="errMsg")}')

# collect vendor
vender_list += list(soup.find_all("mjres:Vendor"))

# check status to handle continue or break
total_response = int(status.get(key="totalRes"))
first_response = int(status.get(key="firstRes"))
total_response_returned = int(status.get(key="totalResRet"))
last_response = first_response + total_response_returned - 1
start_item = last_response + 1
if total_response <= last_response:
    print(f"Received all results. ({last_response} / {total_response})")
else:
    print(f"Received {last_response} results. Continue to receive more. (All: {total_response})")

```

import pytest
from jvn import jvn_api

ENABLE_TEST = False
SKIP_IF = pytest.mark.skipif(
    not ENABLE_TEST,
    reason='access to jvn server should be limited',
)


@SKIP_IF
class TestVulnDetailInfoApi:
    def test_get_vuln_overview(self):
        # given
        api = jvn_api.VulnDetailInfoApi()
        # when
        vuln_detail_info = api.get_vuln_detail_info(vuln_id='JVNDB-2021-017437')
        # then
        assert vuln_detail_info.__dict__ == {
            'vuln_id': 'JVNDB-2021-017437',
            'product': 'mermaid',
            'vendor': 'mermaid project',
            'title': 'Mermaid におけるクロスサイトスクリプティングの脆弱性',
            'version': '8.13.8 未満',
            'description': '\nMermaid には、クロスサイトスクリプティングの脆弱性、および入力確認に関する脆弱性が存在します。\n',
            'impact': '\n情報を取得される、および情報を改ざんされる可能性があります。\n',
            'solution': '\nベンダより正式な対策が公開されています。ベンダ情報を参照して適切な対策を実施してください。\n'
        }


@SKIP_IF
class TestVulnOverviewListApi:
    def test_get_vuln_overview(self):
        # given
        api = jvn_api.VulnOverviewListApi()
        # when
        vuln_overview = api.get_vuln_overview_list(keyword="plotly")
        # then
        assert len(vuln_overview) == 3

import datetime
import pathlib
from jvn.jvn_api.vuln_overview_list_xml import VulnOverviewListXml

SELF_FILE = pathlib.Path(__file__).resolve()
DATA_FOLDER = SELF_FILE.parent.joinpath("data").resolve()


class TestVulnOverviewListXml:
    def test_parse(self):
        # given
        xml = DATA_FOLDER.joinpath(
            "VulnOverviewListRequest__get_xml__mermaid.txt"
        ).read_text(encoding='utf-8')
        parser = VulnOverviewListXml(xml=xml)
        # when
        vuln_overview_list = parser.parse()
        # then
        assert len(vuln_overview_list) == 7
        assert vuln_overview_list[0].__dict__ == {
            'vuln_id': 'JVNDB-2021-017437',
            'product': 'mermaid',
            'vendor': 'mermaid project',
            'title': 'Mermaid におけるクロスサイトスクリプティングの脆弱性',
            'link': 'https://jvndb.jvn.jp/ja/contents/2021/JVNDB-2021-017437.html',
            'description': 'Mermaid には、クロスサイトスクリプティングの脆弱性、および入力確認に関する脆弱性が存在します。',
            'creator': 'Information-technology Promotion Agency, Japan',
            'date': datetime.datetime(2023, 1, 18, 14, 42, 56, tzinfo=datetime.timezone(datetime.timedelta(seconds=32400))),
            'issued': datetime.datetime(2023, 1, 18, 14, 42, 56, tzinfo=datetime.timezone(datetime.timedelta(seconds=32400))),
            'modified': datetime.datetime(2023, 1, 18, 14, 42, 56, tzinfo=datetime.timezone(datetime.timedelta(seconds=32400)))
        }
        assert vuln_overview_list[-1].__dict__ == {
            'vuln_id': 'JVNDB-2006-003518',
            'product': 'mermaid module',
            'vendor': 'PHPNUKE',
            'title': 'PHP-Nuke 用の Mermaid モジュールにおける PHP リモートファイルインクルージョンの脆弱性',
            'link': 'https://jvndb.jvn.jp/ja/contents/2006/JVNDB-2006-003518.html',
            'description': 'PHP-Nuke 用の Mermaid モジュールの formdisp.php には、PHP リモートファイルインクルージョンの脆弱性が存在します。',
            'creator': 'Information-technology Promotion Agency, Japan',
            'date': datetime.datetime(2012, 12, 20, 18, 2, 41, tzinfo=datetime.timezone(datetime.timedelta(seconds=32400))),
            'issued': datetime.datetime(2012, 12, 20, 18, 2, 41, tzinfo=datetime.timezone(datetime.timedelta(seconds=32400))),
            'modified': datetime.datetime(2012, 12, 20, 18, 2, 41, tzinfo=datetime.timezone(datetime.timedelta(seconds=32400)))
        }

import pytest

import pathlib
from jvn.jvn_api.vuln_detail_info_xml import VulnDetailInfoXml

SELF_FILE = pathlib.Path(__file__).resolve()
DATA_FOLDER = SELF_FILE.parent.joinpath("data").resolve()


class TestVulnDetailInfoXml:
    def test_parse(self):
        # given
        xml = DATA_FOLDER.joinpath(
            "VulnDetailInfoRequest__get_xml__JVNDB-2021-017437.txt"
        ).read_text(encoding='utf-8')
        parser = VulnDetailInfoXml(xml=xml)
        # when
        vuln_detail_info = parser.parse()
        # then
        assert vuln_detail_info.__dict__ == {
            'vuln_id': 'JVNDB-2021-017437',
            'product': 'mermaid',
            'vendor': 'mermaid project',
            'title': 'Mermaid におけるクロスサイトスクリプティングの脆弱性',
            'version': '8.13.8 未満',
            'description': '\nMermaid には、クロスサイトスクリプティングの脆弱性、および入力確認に関する脆弱性が存在します。\n',
            'impact': '\n情報を取得される、および情報を改ざんされる可能性があります。\n',
            'solution': '\nベンダより正式な対策が公開されています。ベンダ情報を参照して適切な対策を実施してください。\n'
        }

    @pytest.mark.parametrize("testcase, filename, expect", [
        (
            'count 0',
            'VulnDetailInfoRequest__get_xml__count0_dummy.txt',
            'Vuln detail info not found in one response.'
        ),
        (
            'count 2',
            'VulnDetailInfoRequest__get_xml__count2_dummy.txt',
            'Multiple vuln detail info found in one response.'
        ),
    ])
    def test_parse_irregular_count(self, testcase, filename, expect):
        # given
        xml = DATA_FOLDER.joinpath(filename).read_text(encoding='utf-8')
        parser = VulnDetailInfoXml(xml=xml)
        # when
        # then
        with pytest.raises(RuntimeError) as e:
            parser.parse()
        assert str(e.value) == expect

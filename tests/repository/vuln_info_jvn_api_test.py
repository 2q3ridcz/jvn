from unittest.mock import MagicMock, patch, call

from jvn.repository.vuln_info_jvn_api import VulnInfoJvnApi

from jvn.jvn_api.vuln_overview_list_api import VulnOverviewListApi
from jvn.jvn_api.vuln_detail_info_api import VulnDetailInfoApi
from jvn.entity.vuln_info import VulnInfo


class TestVulnInfoJvnApi:
    def test_get_vuln_info_list(self):
        # given_input
        keywords = [
            'plotly',
            'mermaid',
            'marked',
        ]
        # given_mock
        vuln_id_list = ['JVNDB-2021-017437', 'abc']

        overview_mock_0 = MagicMock()
        overview_mock_0.vuln_id = vuln_id_list[0]

        overview_mock_1 = MagicMock()
        overview_mock_1.vuln_id = vuln_id_list[1]

        detail_mock = VulnInfo(
            vuln_id='JVNDB-2021-017437',
            product='mermaid',
            vendor='mermaid project',
            title='Mermaid におけるクロスサイトスクリプティングの脆弱性',
            version='8.13.8 未満',
            description='\nMermaid には、クロスサイトスクリプティングの脆弱性、および入力確認に関する脆弱性が存在します。\n',
            impact='\n情報を取得される、および情報を改ざんされる可能性があります。\n',
            solution='\nベンダより正式な対策が公開されています。ベンダ情報を参照して適切な対策を実施してください。\n',
        )

        # when
        with patch.object(
            VulnOverviewListApi,
            'get_vuln_overview_list',
            return_value=[overview_mock_0, overview_mock_1]
        ) as overview_api_mock:
            with patch.object(
                VulnDetailInfoApi,
                'get_vuln_detail_info',
                return_value=detail_mock
            ) as detail_api_mock:
                repo = VulnInfoJvnApi()
                vuln_info_list = repo.get_vuln_info_list(keywords=keywords)
        # then
        assert overview_api_mock.call_args_list == [call(keyword=x) for x in keywords]
        assert detail_api_mock.call_args_list == [call(vuln_id=x) for x in vuln_id_list] * 3
        assert len(vuln_info_list) == 6
        assert vuln_info_list[0] == detail_mock

'''Get response of JVN's getVulnDetailInfo api

[MyJVN - API: getVulnDetailInfo]
(https://jvndb.jvn.jp/apis/getVulnDetailInfo_api_hnd.html)
'''
import requests


class VulnDetailInfoRequest:
    def url(self, vuln_id: str) -> str:
        return (
            f"https://jvndb.jvn.jp/myjvn?method=getVulnDetailInfo&feed=hnd"
            f"&vulnId={vuln_id}"
        )

    def get_xml(self, vuln_id: str):
        url = self.url(vuln_id=vuln_id)
        res = requests.get(url=url, timeout=1)
        return res.text

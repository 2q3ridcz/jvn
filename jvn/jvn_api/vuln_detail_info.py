from dataclasses import dataclass


@dataclass(init=True, eq=True, frozen=True)
class VulnDetailInfo:
    vuln_id: str
    product: str
    vendor: str
    title: str
    version: str
    description: str
    impact: str
    solution: str

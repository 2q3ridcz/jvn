'''Get and parse JVN's getVulnOverviewList api

[MyJVN - API: getVulnOverviewList]
(https://jvndb.jvn.jp/apis/getVulnOverviewList_api_hnd.html)
'''
import typing

from .vuln_overview_list_request import VulnOverviewListRequest
from .vuln_overview_list_xml import VulnOverviewListXml
from .vuln_overview import VulnOverview


class VulnOverviewListApi:
    def get_vuln_overview_list(
            self,
            keyword: str
    ) -> typing.List[VulnOverview]:
        xml = VulnOverviewListRequest().get_xml(keyword=keyword)
        return VulnOverviewListXml(xml=xml).parse()

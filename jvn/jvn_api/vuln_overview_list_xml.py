'''Parse response of JVN's getVulnOverviewList api

[MyJVN - API: getVulnOverviewList]
(https://jvndb.jvn.jp/apis/getVulnOverviewList_api_hnd.html)
'''
import datetime
import typing
from bs4 import BeautifulSoup

from .vuln_overview import VulnOverview


class VulnOverviewListXml:
    def __init__(self, xml: str):
        self.xml = xml

    def _soup(self) -> BeautifulSoup:
        text = self.xml
        soup = BeautifulSoup(text, features='xml')
        return soup

    def parse(self) -> typing.List[VulnOverview]:
        soup = self._soup()
        item_list = []
        for item in soup.find_all(name='rss:item'):
            item_list.append(VulnOverview(**{
                'vuln_id': item.find(name='sec:identifier').text,
                'product': item.find(name='sec:cpe').attrs['product'],
                'vendor': item.find(name='sec:cpe').attrs['vendor'],
                'title': item.find(name='rss:title').text,
                'link': item.find(name='rss:link').text,
                'description': item.find(name='rss:description').text,
                'creator': item.find(name='dc:creator').text,
                'date': datetime.datetime.fromisoformat(
                    item.find(name='dc:date').text
                ),
                'issued': datetime.datetime.fromisoformat(
                    item.find(name='dcterms:issued').text
                ),
                'modified': datetime.datetime.fromisoformat(
                    item.find(name='dcterms:modified').text
                ),
            }))
        return item_list

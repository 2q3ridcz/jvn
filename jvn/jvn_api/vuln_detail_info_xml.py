'''Get response of JVN's getVulnDetailInfo api

[MyJVN - API: getVulnDetailInfo]
(https://jvndb.jvn.jp/apis/getVulnDetailInfo_api_hnd.html)
'''
from bs4 import BeautifulSoup

from .vuln_detail_info import VulnDetailInfo


class VulnDetailInfoXml:
    def __init__(self, xml: str):
        self.xml = xml

    def _soup(self) -> BeautifulSoup:
        text = self.xml
        soup = BeautifulSoup(text, features='xml')
        return soup

    def parse(self) -> VulnDetailInfo:
        soup = self._soup()
        items = soup.find_all(name='vuldef:Vulinfo')
        if not items:
            raise RuntimeError('Vuln detail info not found in one response.')
        if len(items) == 2:
            raise RuntimeError(
                'Multiple vuln detail info found in one response.'
            )

        item = items[0]
        return VulnDetailInfo(**{
            'vuln_id': item.find(name='vuldef:VulinfoID').text,
            'product': item.find(name='vuldef:ProductName').text,
            'vendor': item.find(name='vuldef:Name').text,
            'title': item.find(name='vuldef:Title').text,
            'version': item.find(name='vuldef:VersionNumber').text,
            'description': item.find(name='vuldef:VulinfoDescription').text,
            'impact': item.find(name='vuldef:ImpactItem').text,
            'solution': item.find(name='vuldef:SolutionItem').text,
        })

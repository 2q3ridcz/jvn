from dataclasses import dataclass
import datetime


@dataclass(init=True, eq=True, frozen=True)
class VulnOverview:
    vuln_id: str
    product: str
    vendor: str
    title: str
    link: str
    description: str
    creator: str
    date: datetime.datetime
    issued: datetime.datetime
    modified: datetime.datetime

'''Get response of JVN's getVulnOverviewList api

[MyJVN - API: getVulnOverviewList]
(https://jvndb.jvn.jp/apis/getVulnOverviewList_api_hnd.html)
'''
import requests


class VulnOverviewListRequest:
    def url(self, keyword: str) -> str:
        return (
            f"https://jvndb.jvn.jp/myjvn?method=getVulnOverviewList&feed=hnd"
            f"&rangeDatePublic=n&rangeDatePublished=n"
            f"&rangeDateFirstPublished=n&keyword={keyword}"
        )

    def get_xml(self, keyword: str) -> str:
        url = self.url(keyword=keyword)
        res = requests.get(url=url, timeout=1)
        return res.text

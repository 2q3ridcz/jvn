'''Get and parse JVN's getVulnDetailInfo api

[MyJVN - API: getVulnDetailInfo]
(https://jvndb.jvn.jp/apis/getVulnDetailInfo_api_hnd.html)
'''
from .vuln_detail_info_request import VulnDetailInfoRequest
from .vuln_detail_info_xml import VulnDetailInfoXml
from .vuln_detail_info import VulnDetailInfo


class VulnDetailInfoApi:
    def get_vuln_detail_info(self, vuln_id: str) -> VulnDetailInfo:
        xml = VulnDetailInfoRequest().get_xml(vuln_id=vuln_id)
        return VulnDetailInfoXml(xml=xml).parse()

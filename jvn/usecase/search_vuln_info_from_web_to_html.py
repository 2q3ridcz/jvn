import typing

from ..repository.vuln_info_jvn_api import VulnInfoJvnApi
from ..presenter.vuln_info_table_html_writer import VulnInfoTableHtmlWriter


class SearchVulnInfoFromWebToHtml():
    def __init__(
        self,
        keywords: typing.List[str],
        path: str,
    ):
        self.keywords = keywords
        self.path = path

    def execute(self):
        keywords = self.keywords
        path = self.path

        vuln_info_list = VulnInfoJvnApi().get_vuln_info_list(keywords=keywords)
        VulnInfoTableHtmlWriter(vuln_info_list=vuln_info_list).write(path=path)

import typing

from ..entity.vuln_info import VulnInfo
from ..jinja2_writer.table_html_writer import TableHtmlWriter


class VulnInfoTableHtmlWriter():
    def __init__(
        self,
        vuln_info_list: typing.List[VulnInfo],
    ):
        self.vuln_info_list = vuln_info_list

    def write(
        self,
        path: str,
    ):
        vuln_info_list = self.vuln_info_list
        data = {
            'title': 'vulnerabiliby report',
            'h1': 'vulnerabiliby report',
            'pre_table': None,
            'post_table': None,
            'header': list(vuln_info_list[0].__dict__.keys()),
            'records': [list(x.__dict__.values()) for x in vuln_info_list],
        }

        TableHtmlWriter().write(
            path=path,
            data=data,
        )

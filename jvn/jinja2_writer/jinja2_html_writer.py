import pathlib
import typing

import jinja2


class Jinja2HtmlWriter():
    def write(
        self,
        path: str,
        template: str,
        data: typing.Dict,
    ):
        template_file = pathlib.Path(template).resolve()
        template_folder = template_file.parent

        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(str(template_folder.resolve()))
        )
        tpl = env.get_template(template_file.name)
        rendered = tpl.render(data=data)
        with open(path, 'w', encoding='utf-8') as file:
            file.write(rendered)

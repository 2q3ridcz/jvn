from dataclasses import dataclass


@dataclass(init=True, eq=True, frozen=True)
class VulnInfo:
    vuln_id: str
    product: str
    vendor: str
    version: str
    title: str
    description: str
    impact: str
    solution: str

import typing

from ..jvn_api import VulnOverviewListApi, VulnDetailInfoApi
from ..entity.vuln_info import VulnInfo


class VulnInfoJvnApi:
    def get_vuln_info_list(
            self,
            keywords: typing.List[str]
    ) -> typing.List[VulnInfo]:
        """Search vulnerability info using jvn_api

        Args:
            keywords (typing.List[str]): List of keywords to search via jvn_api

        Returns:
            typing.List[VulnInfo]: List of vulnerability info
        """
        vuln_info_list = []
        for keyword in keywords:
            api = VulnOverviewListApi()
            vuln_overview_list = api.get_vuln_overview_list(keyword=keyword)

            for vuln_overview in vuln_overview_list:
                detail_api = VulnDetailInfoApi()
                vuln_info_list.append(
                    detail_api.get_vuln_detail_info(
                        vuln_id=vuln_overview.vuln_id
                    )
                )
        return [VulnInfo(**(x.__dict__)) for x in vuln_info_list]
